package ru.tsc.kyurinova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.model.User;

import java.util.List;

public interface IUserRepository {

    void add(
            @NotNull final User user
    );

    void update(@NotNull final User user);

    @Nullable
    User findByLogin(
            @NotNull final String login
    );

    @Nullable
    User findByEmail(
            @NotNull final String email
    );

    void removeById(
            @NotNull final String id
    );

    void removeByLogin(
            @NotNull final String login
    );

    void remove(
            @NotNull final User user
    );

    @Nullable
    List<User> findAll(
    );

    void clear(
    );

    @Nullable
    User findById(
            @NotNull final String id
    );

    @Nullable
    @NotNull
    User findByIndex(
            @NotNull final Integer index
    );

    void removeByIndex(
            @NotNull final Integer index
    );

    int getSize(
    );

}
