package ru.tsc.kyurinova.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.dto.Domain;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminDataEndpoint {

    @WebMethod
    @SneakyThrows
    @NotNull
    Domain getDomain(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void setDomain(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "domain", partName = "domain")
                    Domain domain);

    @WebMethod
    @SneakyThrows
    void dataBackupLoad(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataBackupSave(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataBase64Load(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataBase64Save(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataBinaryLoad(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataBinarySave(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataJsonLoadFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataJsonLoadJaxB(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataJsonSaveFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataJsonSaveJaxB(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataXmlLoadFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataXmlLoadJaxBC(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataXmlSaveFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataXmlSaveJaxB(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataYamlLoadFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void dataYamlSaveFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    );
}
